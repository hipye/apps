--There are two functions that will install mods, ServerModSetup and ServerModCollectionSetup. Put the calls to the functions in this file and they will be executed on boot.

--ServerModSetup takes a string of a specific mod's Workshop id. It will download and install the mod to your mod directory on boot.
	--The Workshop id can be found at the end of the url to the mod's Workshop page.
	--Example: http://steamcommunity.com/sharedfiles/filedetails/?id=350811795
	--ServerModSetup("350811795")

--ServerModCollectionSetup takes a string of a specific mod's Workshop id. It will download all the mods in the collection and install them to the mod directory on boot.
	--The Workshop id can be found at the end of the url to the collection's Workshop page.
	--Example: http://steamcommunity.com/sharedfiles/filedetails/?id=379114180
	--ServerModCollectionSetup("379114180")

--ServerModSetup("")
ServerModSetup("1216718131")
--ani
ServerModSetup("378160973")
--定位
ServerModSetup("462434129")
--复活
ServerModSetup("2784048339")
--地窖
ServerModSetup("2032577827")
--雪球机
ServerModSetup("785295023")
--超级墙
ServerModSetup("375859599")
--生命值
ServerModSetup("2964299587")
--伤害显示
--ServerModSetup("836583293")
--物品详细
ServerModSetup("666155465")
--SHOWME
ServerModSetup("2798599672")
--背包扩展
ServerModSetup("362175979")
--虫洞颜色