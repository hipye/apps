#!/bin/bash

# 下载 ChmlFrp 压缩包
curl -o ChmlFrp-0.51.2_linux_arm.tar.gz "https://git.u103357.nyat.app:41200/download?file=ChmlFrp-0.51.2_linux_arm.tar.gz"

# 解压压缩包
tar zxvf ChmlFrp-0.51.2_linux_arm.tar.gz -C /root/

# 创建启动脚本 /etc/init.d/chmlfrpc
touch /etc/init.d/chmlfrpc
cat <<EOF > /etc/init.d/chmlfrpc
#!/bin/sh /etc/rc.common

USE_PROCD=1
START=99
STOP=10

SERVICE_NAME="chmlfrpc"
SERVICE_BIN="/root/frp_ChmlFrp-0.51.2_linux_arm/frpc"
CONFIG_DIR="/root/frp_ChmlFrp-0.51.2_linux_arm"

start_service() {
    echo "Starting $SERVICE_NAME instances..."
    
    # 第一个实例
    procd_open_instance
    procd_set_param command "$SERVICE_BIN" -c "$CONFIG_DIR/frpc.ini"
    procd_set_param respawn  # 崩溃后自动重启
    procd_set_param stdout 1  # 输出到系统日志
    procd_set_param stderr 1
    procd_close_instance

    # 第二个实例
    procd_open_instance
    procd_set_param command "$SERVICE_BIN" -c "$CONFIG_DIR/frpc_chick.ini"
    procd_set_param respawn
    procd_set_param stdout 1
    procd_set_param stderr 1
    procd_close_instance

    # 第三个实例
    procd_open_instance
    procd_set_param command "$SERVICE_BIN" -c "$CONFIG_DIR/frpc_chick_hz.ini"
    procd_set_param respawn
    procd_set_param stdout 1
    procd_set_param stderr 1
    procd_close_instance

    # 第四个实例
    procd_open_instance
    procd_set_param command "$SERVICE_BIN" -c "$CONFIG_DIR/frpc_hlbe.ini"
    procd_set_param respawn
    procd_set_param stdout 1
    procd_set_param stderr 1
    procd_close_instance
}

stop_service() {
    echo "Stopping all $SERVICE_NAME instances..."
    killall frpc  # 终止所有frpc进程
}

restart() {
    stop
    start
}
EOF

# 赋予启动脚本执行权限
chmod +x /etc/init.d/chmlfrpc

# 启用并启动服务
/etc/init.d/chmlfrpc enable
/etc/init.d/chmlfrpc start

echo "ChmlFrp 安装和配置完成！"

