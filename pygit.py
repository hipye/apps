import requests
import subprocess
import re
import socket
import dns.resolver

def get_ips_from_ipinfo(domain):
    """使用 ipinfo.io 获取域名的 IP 地址列表"""
    try:
        response = requests.get(f"https://ipinfo.io/{domain}/json")
        data = response.json()
        if 'ip' in data:
            return [data['ip']]  # 返回 IP 地址列表
        else:
            print(f"未从 ipinfo.io 找到 {domain} 的 IP 地址")
            return []
    except Exception as e:
        print(f"无法从 ipinfo.io 获取 IP 地址: {e}")
        return []

def get_ips_from_socket(domain):
    """使用 socket 获取域名的 IP 地址列表"""
    try:
        ips = socket.gethostbyname_ex(domain)[2]
        return ips
    except socket.gaierror as e:
        print(f"无法通过 socket 解析域名: {e}")
        return []

def get_ips_from_dnspython(domain):
    """使用 dnspython 获取域名的 IP 地址列表"""
    try:
        answers = dns.resolver.resolve(domain, 'A')
        ips = [answer.to_text() for answer in answers]
        return ips
    except Exception as e:
        print(f"无法通过 dnspython 解析域名: {e}")
        return []

def get_ips_from_google_dns(domain):
    """使用 Google 公共 DNS 获取域名的 IP 地址列表"""
    try:
        response = requests.get(f"https://dns.google/resolve?name={domain}&type=A")
        data = response.json()
        ips = [answer['data'] for answer in data.get('Answer', []) if answer['type'] == 1]
        return ips
    except Exception as e:
        print(f"无法从 Google DNS 获取 IP 地址: {e}")
        return []

def get_ips_from_github_api():
    """使用 GitHub 官方 API 获取其服务的 IP 地址列表"""
    try:
        response = requests.get("https://api.github.com/meta")
        data = response.json()
        return data.get('web', [])  # 返回 GitHub Web 服务的 IP 地址列表
    except Exception as e:
        print(f"无法从 GitHub API 获取 IP 地址: {e}")
        return []

def get_all_ips(domain):
    """从多个来源获取域名的 IP 地址列表"""
    ips = (
        get_ips_from_ipinfo(domain)
        + get_ips_from_socket(domain)
        + get_ips_from_dnspython(domain)
        + get_ips_from_google_dns(domain)
        + get_ips_from_github_api()
    )
    return list(set(ips))  # 去重

def ping_ip(ip):
    """Ping 一个 IP 地址并返回平均响应时间"""
    try:
        output = subprocess.check_output(["ping", "-c", "4", ip], universal_newlines=True)
        match = re.search(r"min/avg/max/mdev = [\d.]+/([\d.]+)/[\d.]+/[\d.]+ ms", output)
        if match:
            return float(match.group(1))
        else:
            return None
    except subprocess.CalledProcessError:
        return None

def find_fastest_ip(domain):
    """查找访问指定域名最快的 IP 地址"""
    # 获取 IP 地址列表
    ips = get_all_ips(domain)
    if not ips:
        print(f"没有找到 {domain} 的可用 IP 地址")
        return None, None  # 返回两个 None 值

    fastest_ip = None
    fastest_time = float('inf')

    # 测试每个 IP 的响应时间
    for ip in ips:
        print(f"正在测试 IP: {ip}...")
        avg_time = ping_ip(ip)
        if avg_time is not None:
            print(f"IP: {ip}, 平均响应时间: {avg_time} ms")
            if avg_time < fastest_time:
                fastest_time = avg_time
                fastest_ip = ip
        else:
            print(f"IP: {ip} 无法访问")

    return fastest_ip, fastest_time

if __name__ == "__main__":
    domain = "github.com"
    print(f"正在查找 {domain} 的最快 IP 地址...")
    fastest_ip, fastest_time = find_fastest_ip(domain)
    if fastest_ip:
        print(f"最快的 IP 地址是: {fastest_ip}, 平均响应时间: {fastest_time} ms")
    else:
        print("没有找到可用的 IP 地址")